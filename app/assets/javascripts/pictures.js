// Javascript for Picture Gallery

function init_pictures() {
  
  // --- zoom into thumbnail
  $('#zoom_gallery .img-container img').click(function() {
    var img_src =  $(this).attr('src').replace('/thumb/', '/big/');
    $('#zoom_gallery .zoom-image img').attr('src', img_src);
  });
}