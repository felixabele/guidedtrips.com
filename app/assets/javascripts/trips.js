// ============================
//  TRIPS
// ============================

function get_center( arr, ind ) {
  var sum = 0;
  for (var i=0; i<arr.length; i++) {
    sum += parseFloat( arr[i].latLng[ind] );
  }
  return (sum / arr.length);
}


function init_trips() {


  // Google Maps, TODO: refactor
  var add_arr = [];
  $('#trips_result .trip').each(function() {
    var obj = $(this);
    add_arr.push( {latLng:[obj.data('lat'), obj.data('lon')], data: obj.find('h4 a').html()} );
  });

  try {
     $(".gmap4").gmap3({
      map:{
        options:{
          center: [get_center(add_arr, 0), get_center(add_arr, 1)]
        }
      },
      marker:{
        values: add_arr,
        options:{
          draggable: false
        },
        events:{
          mouseover: function(marker, event, context){
            var map = $(this).gmap3("get"),
              infowindow = $(this).gmap3({get:{name:"infowindow"}});
            if (infowindow){
              infowindow.open(map, marker);
              infowindow.setContent(context.data);
            } else {
              $(this).gmap3({
                infowindow:{
                  anchor:marker,
                  options:{content: context.data}
                }
              });
            }
          },
          mouseout: function(){
            var infowindow = $(this).gmap3({get:{name:"infowindow"}});
            if (infowindow){
              infowindow.close();
            }
          }
        }
      }
    }); 
  } catch( e ) {  }

  // --- Handle Tags for a trip 
  $('.handle_tag').click(function(e) {
    e.preventDefault();
    var tag_el = $(this), 
        method = 'POST';

    // remove tag or add tag
    if (tag_el.hasClass('label-success'))
      method = 'DELETE'

    $.ajax({
      url: tag_el.attr('href'),
      type: method,
    }).done(function( data ) {
      if (data.success)
        tag_el.toggleClass('label-success');
    });
  });


  // ===========================================
  // --- Pager Gallery
  // ===========================================
  var bxslider = $('.bxslider').bxSlider({pager: false});  

  $('#picture_pager img').click(function(e) {
    e.preventDefault();    
    bxslider.goToSlide( $(this).data('slide-index') );
  })

  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 160,
    itemMargin: 5
  });

  // ===========================================
  // --- selectable tags
  // ===========================================
  $('#trip_tags').toggleTags({
    selectedClass: 'label-success',
    unselectedClass: 'label-default',
    onSelect: function( el ) {
      el.find('i').removeClass('icon-check-empty').addClass('icon-check');
    },
    onUnselect: function( el ) {
      el.find('i').removeClass('icon-check').addClass('icon-check-empty');
    }
  });

  // ===========================================
  // --- Tab-navigation
  // ===========================================
  /*
  $('.nav-tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    if ($(this).attr('href') == '#pictures_tab') {
      bxslider.reloadSlider();
    } else if ($(this).attr('href') == '#comments_tab') {
      init_comments( 'disabled' );
    }
  });  
*/
  init_comments( 'disabled' );
}

init_trips();