// Javascript for Comments

function init_comments() {

  // --- ajaxified comments
  var commentable_c = $('#comments_for');
  commentable_c.ready(function() {
    var c_id = commentable_c.data('commentableId'),
        c_type = commentable_c.data('commentableType');

    // load all links within this container by ajax
    commentable_c.on('click', 'a', function(e) {
      if(!$(this).hasClass('remote-modal')) {
        e.preventDefault();
        commentable_c.load( $(this).attr('href') );
      }
    });

    commentable_c.load( '/'+ c_type +'/'+ c_id +'/comments', function() {
      //  $('.star').rating();
    });
  });
}