module ApplicationHelper

	# Devise Login Stuff
	def resource_name
		:user
	end

	def resource
		@resource ||= User.new
	end

	def devise_mapping
		@devise_mapping ||= Devise.mappings[:user]
	end

	# Bootstrap helper
	def icon_link_to label, url, icon, opt={}
		link_to icon_label(label, icon), url, opt
	end

	def icon_label label, icon
		"<i class=\"icon-#{icon}\"></i> #{label}".html_safe
	end

	# --- handle flash messages
	def show_flash_notice fl=nil
		html = "<div class='flash_msg container'>"		
		unless fl.nil?
			if msg = fl.alert || fl.notice
				html << "<div class=\"alert alert-#{fl.key?(:alert) ? 'danger' : 'success' }\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>#{msg}</div>"
			end
		end
		html << "</div>"
		html.html_safe
	end

	# Display Active Record Validation errors
	def show_errors obj
		html =  "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"
		html << "<h4>#{pluralize(obj.errors.count, "error")} prohibited this #{obj.class.name} from being saved:</h4>"
		html << "<ul>"
		@trip.errors.full_messages.each{ |msg| html << "<li>#{msg}</li>" }
		html << "</ul></div>"
		html.html_safe
	end

	# google maps
	def draw_a_map location
		content_tag( :div, '', :class => 'gmap3', :data => {:lat => location.latitude, :lng => location.longitude} ).html_safe
	end

	# Grey content box
	def namend_box(name, &block)
		cl = name.parameterize.underscore.to_sym
		html = ""
	  html << "<div class='box #{cl}'>#{capture(&block)}</div>"
	  html << "<style type=\"text/css\">.#{cl}:after {content: '#{name}'}</style>"
	  html.html_safe
	end

	# star rating
	def star_rating count, group, selected=0, is_disabled=false
		html = ""
		count.times do |nb|
			nb_t = nb+1
			html << radio_button_tag( group, nb_t, (selected == nb_t), {:class => "star", :disabled => is_disabled, :value => nb_t} )
		end
		html.html_safe
	end

	def star_rating_outp vote
		trans_vote = vote/2.0
		html = "<ul class='list-unstyled comment-rating'>"
		(1..5).each do |nb|
			
			icon = if nb == trans_vote.round and trans_vote.round != trans_vote.to_i
				'star-half-empty'
			elsif nb <= trans_vote
				'star'
			else 
				'star-empty'
			end
			html << "<li><i class='icon-#{icon}'></i></li>"
		end
		html << "</ul>"
		html.html_safe
	end

	# breadcrumps
	def breadcrumps page_title, links=[]
		links_html = ""
		links.each{|link| links_html << "<li>#{link}</li>" }
		links_html << "<li class='active'>#{page_title}</li>"
		"<div class='breadcrumbs margin-bottom-40'>
		    <div class='container'>
		        <h1 class='pull-left'>#{page_title}</h1>
		        <ul class='pull-right breadcrumb'>#{links_html}</ul>
		    </div>
		</div>".html_safe
	end

	# content panel
	def content_panel(title, color='blue', &block)
	  p_head = "<div class='panel-heading'><h3 class='panel-title'>#{title}</h3></div>"
	  str = "<div class='panel panel-#{color} margin-bottom-40'>#{p_head}<div class='panel-body'>" + capture(&block) + "</div></div>"
	  str.html_safe
	end

	# number to currency with different currencies
	# number_to_multi_currency(10, 'EUR')
	def number_to_multi_currency nb, curr, opt={}
		number_to_currency nb, opt.merge({locale: "currencies.#{curr.downcase}"})
	end

	# --- retrives the selected (or defualt) currency
	def users_currency
		session[:currency].nil? ? AppConfig.default_currency : session[:currency]
	end

end
