module CommentsHelper
  
  # Ajaxified comments loader
  def comments_for obj
    content_tag :div, "", :id => 'comments_for', :data => { :commentable_type => obj.class.table_name, :commentable_id => obj.id }
  end
end
