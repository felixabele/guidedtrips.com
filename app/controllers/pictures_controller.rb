class PicturesController < ApplicationController

  before_filter :authenticate_user!, :except => [:show]
  layout 'modal', :only => [:show]


  # --- show picture in fullsize mode
  # currently not used
  def show
  #  @zoom_pic = Picture.find params[:id]
  #  @all_pictures = Picture.where( :trip_id => @zoom_pic.trip_id )
  end

  # --- Upload a Picture
  def create

    @pic = Picture.new( params[:picture] )
    @pic.user_id = current_user.id
    @pic.agency_image = current_user.is_agency?  

    if @pic.save
      flash[:notice] = t('general.successfully_saved')
    else
      flash[:error] = @pic.errors.full_messages.join(', ')
    end
    goto
  end

  # --- delete a picture (if you have the right to do so)
  def destroy
    @pic = current_user.pictures.find( params[:id] )
    if @pic.delete
      flash[:notice] = t('general.successfully_destroyed')
    else
      flash[:error] = t('general.no_rights')
    end      
    goto
  end

  # --- set this picture as preview
  def use_as_index
    @pic = current_user.pictures.find( params[:id] )
    @pic.update_attribute(:use_for_index, true)
    goto
  end

  private 

  # --- return to where you come from
  def goto
    if current_user.is_agency?
      redirect_to edit_trip_path( @pic.trip, :anchor => 'pictures' )
    else  
      redirect_to @pic.trip
    end
  end
end
