class User::CommentsController < ApplicationController

  before_filter :authenticate_user!

  # --- list
  # GET /users/:id/comments
  def index
    @comments = current_user.comments
    #render :index, :layout => nil
  end

end
