class User::PicturesController < ApplicationController

  before_filter :authenticate_user!

  # --- list
  # GET /users/:id/comments
  def index
    @grouped_pictures = current_user.pictures.group_by {|p| p.trip}
  end

end
