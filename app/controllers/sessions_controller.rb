class SessionsController < Devise::SessionsController
  
  layout 'modal', :only => [:new_as_modal]

  # GET /modal_sign_in
  def new_as_modal

  end

end