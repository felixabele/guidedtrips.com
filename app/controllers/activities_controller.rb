class ActivitiesController < ApplicationController

	layout 'modal', :except => [:create, :destroy]

  # --- edit
  def edit
    get_activity
  end

  def update
    get_activity
    if @activity.update_attributes( params[:activity] )
      resp = {:success => true, :msg => t('general.successfully_updated'), :redirect_to => "/trips/#{@trip.id}"}
    else
      resp = {:success => false, :msg => @activity.errors.full_messages.join(', ')}
    end

    respond_to do |format|
      format.json { render json: resp.to_json }
    end
  end


  # --- new
  def new
  	@activity = Activity.new
    trip = Trip.find( params[:trip_id] )
    @activity.day = params[:day]
    @activity.trip_id = trip.id
  end

  def create
    activity = Activity.new params[:activity]    
    
    if activity.save
      resp = {:success => true, :msg => t('general.successfully_saved'), :redirect_to => "/trips/#{activity.trip_id}"}
    else
      resp = {:success => false, :msg => activity.errors.full_messages.join(', ')}
    end

    respond_to do |format|
      format.json { render json: resp.to_json }
    end
  end

  # --- Destroy
  def destroy
    get_activity    
    flash[:notice] = t('general.successfully_destroyed') if @activity.destroy
    redirect_to @trip
  end

  private 

  def get_activity
    @activity = current_user.activities.find( params[:id] )
    @trip = @activity.trip
  end
end
