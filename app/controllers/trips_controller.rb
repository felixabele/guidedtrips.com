class TripsController < ApplicationController
  
  before_filter :authenticate_user!, :only => [:my_trips, :new, :edit, :create, :update, :destroy]
  layout nil, :only => :search  

  # GET /trips
  def index
    get_results
    @search_mode = params['search-mode'] || 'location'
  end

  # Ajax, GET /trips  
  def search
    get_results    
    render :partial => 'list'
  end

  # --- list my trips as Agency
  def my_trips
    @trips = current_user.trips
  end


  # GET /trips/1
  def show
    @trip = Trip.find(params[:id])
  end

  # GET /trips/new
  def new
    @trip = Trip.new :user_id => current_user.id
  end

  # GET /trips/1/edit
  def edit
    @trip = current_user.trips.find(params[:id])
    @my_tags = @trip.tags.collect{|tag| tag.name }
  end

  # POST /trips
  def create
    @trip = Trip.new(params[:trip])
    @trip.user_id = current_user.id

    respond_to do |format|
      if @trip.save
        format.html { redirect_to @trip, notice: 'Trip was successfully created.' }
        format.json { render json: @trip, status: :created, location: @trip }
      else
        format.html { render action: "new" }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /trips/1
  # PUT /trips/1.json
  def update
    @trip = current_user.trips.find(params[:id])

    respond_to do |format|
      if @trip.update_attributes(params[:trip])
        format.html { redirect_to @trip, notice: 'Trip was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trips/1
  # DELETE /trips/1.json
  def destroy
    @trip = current_user.trips.find(params[:id])
    @trip.destroy

    respond_to do |format|
      format.html { redirect_to trips_url }
      format.json { head :no_content }
    end
  end

  # POST /trips/1/add_tag?tag=<tag_name>
  # DELETE /trips/1/remove_tag?tag=<tag_name>
  def handle_tag
    get_trip
    
    if request.post?
      @trip.tag_list.add( params[:tag] )
    elsif request.delete?
      @trip.tag_list.remove( params[:tag] )
    end
    @trip.save!    
    render json: {:success => true}.to_json
  end

  private 

  def get_trip
    @trip = current_user.trips.find( params[:id] ) 
  end

  # --- General Trip Search
  def get_results

    days_count = params[:days].to_i
    @location = Location.find(params[:location_id]) if params[:location_id]

    # fulltext search
    if params[:search] or params[:location_id]
      search = Trip.search do

        # fulltext or location based search
        if @location.present?
          max_km = 100
          with(:location).in_radius(@location.latitude, @location.longitude, max_km, :bbox => true)
        elsif params[:search].present?
          fulltext params[:search] do
            query_phrase_slop 1
          end
        end

        with(:tag_list).any_of(params[:tags].split(',')) if params[:tags].present?

        # how many days?
        if [1,2,3].include? days_count
          with(:days, days_count)
        elsif days_count == 4
          with(:days).less_than(7)
        elsif days_count == 5
          with(:days).greater_than(6)
        end

        # maximum price
        with(:price).less_than(params[:price].to_i) if params[:price].present?
      end
      @trips = search.results
    end

    @trips ||= Trip.published.all
  end

end
