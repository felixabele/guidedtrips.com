class ConfigurationsController < ApplicationController
  
  # PUT /config/set_currency
  def set_currency
    session[:currency] = params[:currency] if AppConfig.major_currencies.include? params[:currency]
    return_back
  end

  private 

  def return_back
    redirect_to session[:previous_url] || root_path
  end

end