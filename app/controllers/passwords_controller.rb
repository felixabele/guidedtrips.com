class PasswordsController < Devise::PasswordsController


  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      resp = {success: true, msg: t('general.successfully_updated'), redirect_to: after_sending_reset_password_instructions_path_for(resource_name)}
    else
      resp = {success: false, msg: resource.errors.full_messages.join(', ')}
    end

    respond_to do |format|
      format.json { render json: resp.to_json }
    end    
  end

end