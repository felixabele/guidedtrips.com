# this controller is related to the current user

class UserController < ApplicationController

  layout nil

  # PUT /user/avatar
  def update_avatar
    redirect_to( edit_user_registration_path )
  end
end