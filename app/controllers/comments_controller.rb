class CommentsController < ApplicationController

	layout 'modal', :except => [:create, :destroy, :index]

  # --- list
  # GET /trips/:id/comments
  def index
    @comments = Trip.find(params[:trip_id]).comments.includes( :user ).page params[:page]
    render :index, :layout => nil
  end

  # --- edit
  def edit
    get_comment
  end

  def update
    get_comment
    if @comment.update_attributes( params[:comment] )
      resp = {:success => true, :msg => t('general.successfully_updated'), :redirect_to => "/trips/#{@trip.id}"}
    else
      resp = {:success => false, :msg => @comment.errors.full_messages.join(', ')}
    end

    respond_to do |format|
      format.json { render json: resp.to_json }
    end
  end


  # --- new
  def new
  	@comment = Comment.new
    trip = Trip.find( params[:trip_id] )
    @comment.trip_id = trip.id
  end

  def create
    comment = Comment.new params[:comment]
    comment.user_id = current_user.id    
    if comment.save
      resp = {:success => true, :msg => t('general.successfully_saved'), :redirect_to => "/trips/#{comment.trip_id}"}
    else
      resp = {:success => false, :msg => comment.errors.full_messages.join(', ')}
    end

    respond_to do |format|
      format.json { render json: resp.to_json }
    end
  end

  # --- Destroy
  def destroy
    get_comment    
    flash[:notice] = t('general.successfully_destroyed') if @comment.destroy
    redirect_to @trip
  end

  private 

  def get_comment
    @comment = current_user.comments.find( params[:id] )
    @trip = @comment.trip
  end
end
