class LocationsController < ApplicationController
  
  # GET /locations/:country
  # GET /locations/:country.json
  # Params: search_string, rows
  def show
    @locations = Location.for_autocomplete( params[:country], params[:search_string], params[:rows] ) 

    respond_to do |format|
      format.html { 
        render text: @locations.collect {|loc| "#{loc.accent_city}<br />" } 
      }
      format.js { 
        render json: (@locations.collect {|loc| {:label => loc.accent_city, :label => loc.accent_city, :id => loc.id } }).to_json
      }
    end
  end

end
