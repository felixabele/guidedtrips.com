class VideosController < ApplicationController

  before_filter :authenticate_user! #, :only => [:new, :edit, :create, :update, :destroy]
  layout 'modal', :only => [:show]

  # --- Upload a Picture
  def create

    @video = Video.new( params[:video] )
    @video.user_id = current_user.id
    @video.belongs_to_agency = current_user.is_agency?  

    if @video.save
      flash[:notice] = t('general.successfully_saved')
    else
      flash[:error] = @video.errors.full_messages.join(', ')
    end
    goto
  end

  # --- open video player
  def show
    @video = Video.find( params[:id] )
  end

  # --- delete a picture (if you have the right to do so)
  def destroy
    @video = current_user.videos.find( params[:id] )
    if @video.delete
      flash[:notice] = t('general.successfully_destroyed')
    else
      flash[:error] = t('general.no_rights')
    end      
    goto
  end

  private 

  # --- return to where you come from
  def goto
    if current_user.is_agency?
      redirect_to edit_trip_path( @video.trip, :anchor => 'videos' )
    else  
      redirect_to @video.trip
    end
  end
end
