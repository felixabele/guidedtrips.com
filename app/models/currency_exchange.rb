# https://github.com/RubyMoney/money#currency-exchange
# https://github.com/RubyMoney/eu_central_bank

module CurrencyExchange

  EXCHANGE_RATES_FILE = "#{Rails.root}#{AppConfig.exchange_rates_file}"

  
  # convert the field 'price'
  def convert_price_to new_currency="EUR"
    exchange( self.price, "USD", new_currency )
  end

  # set the field 'price' with any currency 
  def set_price_as new_price, new_currency
    self.price = exchange( new_price, new_currency, AppConfig.default_currency )
  end

  # returns the price as type Money  
  def price_as_money 
    Money.new(self.price, (self.currency || AppConfig.default_currency))
  end

  private

  # performs actual money exchange
  def exchange( amount, from_currency, to_currency )
    eu_bank = EuCentralBank.new
    eu_bank.update_rates( EXCHANGE_RATES_FILE )
    eu_bank.exchange(amount, from_currency, to_currency).cents
  end  

  # --- converts currency and price to the default
  # called before saving
  def convert_currency
    if self.currency.present? and self.currency != AppConfig.default_currency
      set_price_as self.price, self.currency
    end
  end
end