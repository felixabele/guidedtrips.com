# == Schema Information
#
# Table name: trips
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  description      :text
#  location_id      :integer
#  included         :text
#  required         :text
#  price            :float
#  min_participants :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#  excerpt          :text
#  is_published     :boolean          default(FALSE)
#

class Trip < ActiveRecord::Base

  include CurrencyExchange
  extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :description, :included, :location_id, :min_participants, :name, :price, :required, :tag_list, :excerpt, :currency, :is_published
  attr_accessor :currency

  belongs_to :user
  belongs_to :location
  has_one :country, :through => :location
  has_one :agency, :through => :user
  has_many :activities, :dependent => :destroy
  has_many :pictures, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :videos, :dependent => :destroy

  validates :description, :name, :price, :presence => true
  validates :excerpt, length: { maximum: 200 }

  # by country
  scope :by_country, (lambda do |country|
    joins( :location ).where( 'locations.country' => country )
  end)  
  scope :published, where(:is_published => true)

  acts_as_taggable

  before_save :convert_currency


  # -------------------------------
  # => Search related stuff
  # -------------------------------

  # Solr - Searchable
  searchable :if => :is_published do
    autosuggest :trip_country, :using => :country_name

    latlon(:location) { Sunspot::Util::Coordinates.new(self.location.latitude, self.location.longitude) }

    text :description, :name
    text :location do 
      location.accent_city
    end
    text :country do 
      self.country_name
    end

    string :tag_list, :multiple => true do
      tag_list.map { |t| t}
    end

    integer :location_id

    integer :days do
      self.days
    end

    double :price do
      self.price.to_i
    end       
  end


  # get al activities of a day
  def activities_for_day( day=1 )
  	self.activities.where( :day => day )
  end

  # get number of days
  def days
  	d = self.activities.select( 'DISTINCT day' ).count
    d == 0 ? 1 : d
  end 

  # --- return the long version of the country
  def country_name
    self.country.name
  end

  def country
    self.location.country
  end

  # --- returns the image to use for index
  def picture_for_index
    self.pictures.use_for_index.first || self.pictures.first
  end

  # --- get average user rating
  def average_rating
    votings = self.comments.avg_voting.first
    {:sum => votings.sum, :vote => votings.avg_vote}
  end

  # --- is the passed user the editor?
  def is_the_editor? user
    self.user_id == user.id
  end

  # ---------------------------------
  #   Tag-Clouds
  # ---------------------------------

  # --- Country Cloud / Sum of trips per country
  def self.country_cloud( limit=10 )
    select( 'COUNT(*) AS sum, countries.name, locations.id AS id' ).joins( [:location, :country] ).group( 'countries.name' ).order( 'sum DESC' ).limit( limit )
  end

  # --- Tag Cloud
  # TODO: this belongs to ActsAsTaggableOn
  def self.tag_cloud( records )
    if records.any?
      rec_ids = records.collect {|rec| rec.id}

      ActsAsTaggableOn::Tag
        .select( "name, COUNT(*) AS 'sum'" )
        .where( "taggable_type = 'Trip' AND taggable_id IN (#{rec_ids.join(',')})" )
        .joins( :taggings ).group( :tag_id ).order( 'sum DESC' ) 
    else
      []
    end
  end  
end
