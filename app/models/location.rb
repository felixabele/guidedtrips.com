# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  country_code :string(2)
#  city         :string(120)
#  accent_city  :string(120)
#  region       :string(6)
#  population   :integer
#  latitude     :decimal(15, 10)  default(0.0)
#  longitude    :decimal(15, 10)  default(0.0)
#  country_id   :integer
#

class Location < ActiveRecord::Base
  attr_accessible :accent_city, :city, :country_id, :latitude, :longitude, :population, :region, :country_code
  #attr_accessor :location

  has_many :trips
  belongs_to :country

  # Solr - Searchable
  searchable do
    autosuggest :location_name, :using => :autocomplete_string
  end

=begin
  
  # --- for autocomplete
  def self.for_autocomplete(begins_with, limit=10) 
  	select( ['locations.id', 'accent_city'] )
    .joins( :country )
    .where( "countries.name LIKE :term OR accent_city LIKE :term", {:term => "#{begins_with}%"})
    .limit( limit ).order( 'population DESC' )
  end

  # --- for autocomplete
  def self.autocomplete(term) 
    self.search{ fulltext term }.results
  end
=end

  def country_name
    self.country.name if self.country
  end

  def autocomplete_string
    "#{accent_city} (#{country_name})"
  end

  # --- returns the country as Carmen
  # https://github.com/jim/carmen
  #def country_as_obj
  #  Carmen::Country.coded country
  #end  

  # --- italy insted of it 
  #def l_country_name
  #  country_as_obj.present? ? country_as_obj.name : country
  #end


  def self.set_country_ids
    where(:country_id => nil).each do |l|
      l.country = Country.find_by_code(l.country_code.upcase)
      l.save
    end
    ''
  end
end
