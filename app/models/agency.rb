# == Schema Information
#
# Table name: agencies
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  street      :string(255)
#  city        :string(255)
#  zip         :string(255)
#  country     :string(255)
#  phone       :string(255)
#  email       :string(255)
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Agency < ActiveRecord::Base
	
	belongs_to :user
  has_many :trips, :through => :user
  
  attr_accessible :city, :country, :description, :email, :name, :phone, :street, :user_id, :zip
  # TODO: add attributes: Website

  validates :name, :presence => true, :uniqueness => true


  def full_address
    "#{street}, #{zip} #{city}, #{country}"
  end

  def belongs_to? user
    user.id == self.user_id
  end
end
