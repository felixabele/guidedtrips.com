# == Schema Information
#
# Table name: activities
#
#  id          :integer          not null, primary key
#  trip_id     :integer
#  day         :integer
#  time        :time
#  text        :text
#  location_id :integer
#

class Activity < ActiveRecord::Base
  attr_accessible :day, :location_id, :text, :time, :trip_id, :hour, :minute
  attr_accessor :hour, :minute

  validates :day, :text, :presence => true
  default_scope :order => 'activities.time ASC'
  belongs_to :trip

  # callbacks
  after_initialize :set_accessors
  before_save :transform_time


  private

  def set_accessors
    if self.time
      self.hour = self.time.hour
      self.minute = self.time.min
    end
  end

  def transform_time
    self.time = Time.parse("#{self.hour}:#{self.minute}") if self.hour.present? and self.minute.present?
  end
end
