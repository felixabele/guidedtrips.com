# == Schema Information
#
# Table name: countries
#
#  id             :integer          not null, primary key
#  code           :string(2)
#  name           :string(255)
#  full_name      :string(255)
#  iso3           :string(3)
#  number         :integer
#  continent_code :string(2)
#

class Country < ActiveRecord::Base
  attr_accessible :code, :continent_code, :full_name, :iso3, :name, :number

  has_many :locations
  has_many :trips, :through => :locations
end
