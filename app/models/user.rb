# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  username               :string(255)
#  first_name             :string(255)
#  last_name              :string(255)
#  newsletter             :boolean
#  country                :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :omniauthable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible(
    :email, :password, :password_confirmation, :remember_me, :country,
    :username, :last_name, :first_name, :agency_attributes, 
    :avatar, :avatar_file_name, :avatar_content_type, :avatar_file_size, :avatar_updated_at)

  has_attached_file :avatar, 
    :styles => { :big => "600x600>", :medium => "300x300>", :thumb => "80x100#" }
    #:path => ":rails_root/public/system/:class/:style/:filename"

  # Validations
  validates :username, :last_name, :first_name, :country, :presence => true
  validates :username, :uniqueness => true

  # associations
  has_one :agency, :autosave => true,   :dependent => :destroy
  has_many :trips
  has_many :pictures
  has_many :videos
  has_many :activities, :through => :trips
  has_many :comments, :dependent => :destroy
  has_many :authorizations

  accepts_nested_attributes_for :agency

  # --- Agency related stuff
	def with_agency
	  self.build_agency if agency.nil?
	  self
	end  

  def is_agency?
    self.agency.present?
  end

  # --- return the long version of the country
  def country_name
    Carmen::Country.coded( self.country ).name
  end


  # -------------------------
  # --- OmniAuth stuff
  # -------------------------
  def self.new_with_session(params,session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"],without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def self.from_omniauth(auth, current_user)
    authorization = Authorization.where(:provider => auth.provider, :uid => auth.uid.to_s, :token => auth.credentials.token, :secret => auth.credentials.secret).first_or_initialize
    if authorization.user.blank?
      user = current_user.nil? ? User.where('email = ?', auth["info"]["email"]).first : current_user
      if user.blank?
       user = User.new
       user.password    = Devise.friendly_token[0,10]
       user.first_name  = auth.info.first_name
       user.last_name   = auth.info.last_name
       user.username    = auth.info.nickname
       user.email       = auth.info.email
       user.country     = auth.extra.raw_info.locale.split('_')[1]
       auth.provider == "twitter" ?  user.save(:validate => false) :  user.save
     end
     authorization.username = auth.info.nickname
     authorization.user_id = user.id
     authorization.save
   end
   authorization.user
 end  
end
