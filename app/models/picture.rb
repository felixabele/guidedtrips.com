# == Schema Information
#
# Table name: pictures
#
#  id                :integer          not null, primary key
#  trip_id           :integer
#  user_id           :integer
#  agency_image      :boolean
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  use_for_index     :boolean
#

class Picture < ActiveRecord::Base
  attr_accessible :agency_image, :trip_id, :user_id, :file, :use_for_index

  before_save :check_single_index_use

  has_attached_file :file, 
    :styles => { :big => "600x400>", :medium => "300x200>", :thumb => "300x260#", :fixed_thumb => "160x120#", :sx => "100x80>" }

  belongs_to :trip
  belongs_to :user

  validates :file, :presence => true

  scope :use_for_index, :conditions => {:use_for_index => true, :agency_image => true}
  scope :from_agency, where(:agency_image => true)
  scope :from_traveler, where(:agency_image => false)


  # --- makes sure that only one Agency image is used for index
  def check_single_index_use
    if self.agency_image and self.use_for_index
      if former_index_pic = Picture.use_for_index.where( :trip_id => self.trip_id ).first
        former_index_pic.update_attribute( :use_for_index, false )
      end
    end
  end
  
end
