# == Schema Information
#
# Table name: videos
#
#  id                :integer          not null, primary key
#  key               :string(255)
#  source            :string(255)
#  user_id           :integer
#  trip_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  thumbnail         :string(255)
#  title             :string(255)
#  description       :text
#  is_active         :boolean          default(TRUE)
#  belongs_to_agency :boolean
#

class Video < ActiveRecord::Base
  attr_accessible :key, :source, :trip_id, :user_id

  belongs_to :trip
  before_save :preset_attributes!
  validates :key, :presence => true

  # for now only youtube is supported, TODO: Vimeo, etc..
  def source_url
    "https://www.youtube.com/embed/#{key}"
  end

  def get_api_data
    JSON.parse(open("https://www.googleapis.com/youtube/v3/videos?id=#{key}&part=snippet&key=#{AppConfig.google_api_key}").read)
  end 

  # --- gets all attributes by API
  def preset_attributes!
    data = self.get_api_data

    # video is no longer available
    if data['items'].empty?
      self.is_active = 0
    else
      snipped = data['items'][0]['snippet']
      self.title = snipped['title']
      self.description = snipped['description']
      self.thumbnail = snipped['thumbnails']['default']['url']
      self.is_active = 1
    end
  end
end
