# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  body       :text
#  vote       :integer
#  user_id    :integer
#  trip_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Comment < ActiveRecord::Base
  attr_accessible :body, :trip_id, :user_id, :vote

  validates :body, :presence => true

  belongs_to :user
  belongs_to :trip

  # --- calulate average comment voting
  scope :avg_voting, :select => "AVG(vote) AS avg_vote, COUNT(*) AS sum"

  paginates_per 5

  # --- is the passed user the author?
  def is_the_author? user
    self.user_id == user.id
  end
end
