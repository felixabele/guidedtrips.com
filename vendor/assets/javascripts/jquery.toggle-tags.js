// jQuery toggle-Tags-Plugin
// makes a list of elements toggable and adds their values (html content) to another element as a list
// version 0.1, 26.04.2014
// by Felix Abele

(function($) {

    // ---> ToggleTags-Plugin Start
    $.toggleTags = function(element, options) {

        var $element = $(element),
             element = element;

        // --- Default-Options
        var defaults = {
            element: $element,
            selectedClass: 'selected',
            unselectedClass: 'unselected',
            targetElement: null,
            onSelect: function( el ) {},
            onUnselect: function( el ) {}
        }

        var plugin = this;
        plugin.settings = {};
        plugin.tags = [];
        plugin.target_element;


        // ----------------------------------------
        //      INITIALISATION
        // ----------------------------------------
        plugin.init = function() {

            // Extend Default-Settings with Customs
            plugin.settings = $.extend({}, defaults, options);
            plugin.element = $element;

            // overwrite these preset values with html-5 data-attributes 
            plugin.settings = $.extend({}, plugin.settings, plugin.element.data());
            
            // Set Tag-Elements
            plugin.tags = $element.children();

            // set a target element, if a selector is given
            if (plugin.settings.targetElement != null)
                plugin.target_element = $(plugin.settings.targetElement);

            // Assign Events
            plugin.tags.click(function() {
                toggle_el( $(this) );
            });
        }

        // ----------------------------------------
        //      Private Methods
        // ----------------------------------------

        // selects / un-selects element
        var toggle_el = function( el ) {
            if (el.data('selected')) {
                unselect_el( el );
            } else {
                select_el( el );
            }
        }

        // called when element is selected
        var select_el = function( el ) {
            el.removeClass( plugin.settings.unselectedClass ).addClass( plugin.settings.selectedClass );
            el.data('selected', true);
            plugin.settings.onSelect( el );
            update_values();
        }

        // called when element is unselected
        var unselect_el = function( el ) {
            el.removeClass( plugin.settings.selectedClass ).addClass( plugin.settings.unselectedClass );
            el.data('selected', false);
            plugin.settings.onUnselect( el );
            update_values();
        }

        var update_values = function() {
            var values = [];
            jQuery.each(plugin.element.children('.'+ plugin.settings.selectedClass), function(i,v) {
                values.push($(v).html());
            });
            plugin.element.data('values', values);

            if (plugin.target_element != undefined)
                assign_values_to_target( values );
        }

        // wirtes all values into a target element
        var assign_values_to_target = function( values ) {
            var values_str = values.join(',');
            if (plugin.target_element.is('[value]'))
                plugin.target_element.val( values_str );
            else
                plugin.target_element.html( values_str );
        }
        
        // Start
        plugin.init();
    }            

    // add the plugin to the jQuery.fn object
    $.fn.toggleTags = function(options) {
        return this.each(function() {
            if (undefined == $(this).data('toggleTags')) {
                var plugin = new $.toggleTags(this, options);
                $(this).data('toggleTags', plugin);
            }
        });
    }
})(jQuery);