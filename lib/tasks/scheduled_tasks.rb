class ScheduledTasks

  # --- Update currency change rates
  def self.update_exchange_rates
    eu_bank = EuCentralBank.new
    cache = "#{Rails.root}#{AppConfig.exchange_rates_file}"
    eu_bank.save_rates(cache)
  end


end
