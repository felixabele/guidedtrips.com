FactoryGirl.define do 

	factory :picture do 
				
    file { fixture_file_upload("#{Rails.root}/public/images/test-avatar.jpg", 'text/plain') }
	end
end