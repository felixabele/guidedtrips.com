FactoryGirl.define do 
  factory :country do 
    code 'AF'
    name 'Afghanistan'
    full_name 'Islamic Republic of Afghanistan'
    iso3 'AFG'
    number 4
    continent_code 'AS'
  end
end