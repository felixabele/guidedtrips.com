FactoryGirl.define do 

	factory :trip do 
		
		sequence(:name){|n| "Trip #{n}"}
		description "Some Sample description Text"
		price 100.00

		association :user, :factory => :user
    association :location, :factory => :location

	end
end