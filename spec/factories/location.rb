FactoryGirl.define do 
  # :accent_city, :city, :country, :latitude, :longitude, :population, :region
	factory :location do 
		
		sequence(:city){|n| "City #{n}" }
    latitude 42.50
    longitude 1.51

    association :country
	end
end