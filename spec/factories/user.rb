FactoryGirl.define do 

  #attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :last_name, :first_name, :agency_attributes

	factory :user do 
		
		sequence(:username){|n| "user#{n}"}
		email {|a| "#{a.username}@somemail.de".downcase }
		first_name "Hans"
		last_name "Test A Lot"
	  password 'secret123'
	  password_confirmation 'secret123'		
	  country 'DE'
	end
end