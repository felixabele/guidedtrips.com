FactoryGirl.define do 

	factory :activity do 
		
		sequence(:time){|n| Time.parse('07:00:00') + (n).hour }
		text "Super cool activity"
		day 1
	end
end