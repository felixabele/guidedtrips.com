FactoryGirl.define do 

	factory :agency do 
		
		sequence(:name){|n| "Agency #{n}"}
		city "Berlin"
		country "DE"
	  description "Some Fancy Tour Agency"
	  street 'Easystreet. 16'
	  zip '01927'		

	  association :user, :factory => :user
	end
end