# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  country_code :string(2)
#  city         :string(120)
#  accent_city  :string(120)
#  region       :string(6)
#  population   :integer
#  latitude     :decimal(15, 10)  default(0.0)
#  longitude    :decimal(15, 10)  default(0.0)
#  country_id   :integer
#

# call me with:  bundle exec rspec spec/models/location_spec.rb

require 'spec_helper'

describe Location do
  it "should create a location with country" do
    loc = Factory :location
    puts loc.country.name
    loc.country.should_not be_nil
  end
end
