# == Schema Information
#
# Table name: videos
#
#  id                :integer          not null, primary key
#  key               :string(255)
#  source            :string(255)
#  user_id           :integer
#  trip_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  thumbnail         :string(255)
#  title             :string(255)
#  description       :text
#  is_active         :boolean          default(TRUE)
#  belongs_to_agency :boolean
#

# call me with:  bundle exec rspec spec/models/video_spec.rb

require 'spec_helper'

describe Video do
  it "should return video details from youtube API" do
    video = Factory :video
    video.preset_attributes!
    video.title.should_not be_nil
    video.description.should_not be_nil
    video.thumbnail.should_not be_nil
  end

  it "should disable video when its no longer available" do
    video = Factory :video, key: 'notexistingkey'
    video.preset_attributes!
    video.is_active.should_not be_true
  end  
end
