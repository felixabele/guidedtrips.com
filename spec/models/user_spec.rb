# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  username               :string(255)
#  first_name             :string(255)
#  last_name              :string(255)
#  newsletter             :boolean
#  country                :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#

# call me with:  bundle exec rspec spec/models/user_spec.rb

require 'spec_helper'

describe User do
  
  it "should create a user by factory" do
  	user = Factory :user
  	assert user.valid?
	end

  it "should create an Agency by factory" do
  	a_user = (Factory :agency).user
  	assert a_user.is_agency?
	end	
end
