# == Schema Information
#
# Table name: trips
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  description      :text
#  location_id      :integer
#  included         :text
#  required         :text
#  price            :float
#  min_participants :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#  excerpt          :text
#  is_published     :boolean          default(FALSE)
#

# call me with:  bundle exec rspec spec/models/trip_spec.rb

require 'spec_helper'

describe Trip do

  it "should create a Trip from Factory" do
  	trip = Factory :trip
  	assert trip.valid?
	end

  it "should create a trip with several activities" do
  	trip = Factory :trip
  	5.times do
  		trip.activities << Factory( :activity )
  	end

  	assert_equal 5, trip.activities_for_day( 1 ).count
	end

  it "should count the number of days" do
  	trip = Factory :trip

  	2.times do
  		trip.activities << Factory( :activity, :day => 1 )
  	end

  	2.times do
  		trip.activities << Factory( :activity, :day => 2 )
  	end

  	assert_equal 2, trip.days
	end


  it "should calculate the average comment votes" do
    trip = Factory :trip
    trip.comments << Factory(:comment, :vote => 8)
    trip.comments << Factory(:comment, :vote => 4)
    avg_r = trip.average_rating
    avg_r[:sum].should be 2
    avg_r[:vote].to_i.should be 6
  end


  it "should create a country tag cloud" do

    it = Factory :location, :country => Factory(:country, code: 'IT')
    5.times do
      trip = Factory(:trip, :location_id => it.id)
    end

    de = Factory :location, :country => Factory(:country, code: 'DE')
    3.times do
      trip = Factory(:trip, :location_id => de.id)
    end

    Trip.by_country( 'de' ).count.should be 3
    Trip.country_cloud.each do |ce|
      puts "#{ce.country}: #{ce.sum}"
    end
  end

  # -------------------------------
  # => Fulltext Search (Solr)
  # -------------------------------
  # TODO: Solr Server for test ENV
  # https://github.com/justinko/sunspot-rails-tester
  it "should find a trip within a distance of 100 Km" do

    bremen  = Factory(:location, :latitude => 53.0833330000, :longitude => 8.8000000000 )
    hamburg = Factory(:location, :latitude => 53.5500000000, :longitude => 10.0000000000 )
    trip    = Factory :trip, :location_id => hamburg.id

    search = Trip.search do
      with(:location).in_radius(bremen.latitude, bremen.longitude, 100, :bbox => true)
    end
    results = search.results
    puts results.count
    results.each do |r|
      puts r.id
    end
    #search.results.count.should be 1
  end

  # -------------------------------
  # => Money exchange stuff
  # -------------------------------
  it "should return the trips price as EUR" do
    trip = Factory :trip, :price => 100
    trip.convert_price_to( "EUR" ).should be_within(3).of(73)
  end

  it "should return convert currency before saving" do
    trip = Factory :trip, :price => 100, :currency => 'EUR'
    trip.price.should be_within(10).of(135)
  end
end
