# == Schema Information
#
# Table name: pictures
#
#  id                :integer          not null, primary key
#  trip_id           :integer
#  user_id           :integer
#  agency_image      :boolean
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  use_for_index     :boolean
#

# call me with: rspec spec/models/picture_spec.rb

require 'spec_helper'

describe Picture do
  
  it 'should set a Picture to use for index' do
    trip = Factory :trip
    4.times{ trip.pictures << Factory( :picture, :use_for_index => true, :agency_image => true ) } 
    trip.pictures.count.should be 4
    trip.pictures.use_for_index.count.should be 1
  end
end
