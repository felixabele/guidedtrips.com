GuidedTrips::Application.routes.draw do

  # --- global configurations
  match 'config/:action' => 'configurations', :via => :put, :as => 'config'

  resources :pictures, :only => [:create, :show, :destroy] do 
    member do 
      put :use_as_index      
    end
  end

  resources :videos

  resources :trips do
    
    resources :comments, :only => [:index]

    collection do
      get :search
    end

    member do 
      post :handle_tag
      delete :handle_tag      
    end
  end
  match 'my_trips' => 'trips#my_trips', :as => 'my_trips'
  
  resources :activities
  resources :agencies, :only => [:show]
  resources :comments

  devise_for :users, :controllers => { 
    :registrations => "registrations", 
    :passwords => 'passwords',
    :omniauth_callbacks => "omniauth_callbacks"}
  devise_scope :user do
    get 'modal_sign_in' => 'sessions#new_as_modal'
  end  

  get "static/index"

  # --- Users
  match 'user/avatar' => 'user#update_avatar', :via => :post
  match 'users/edit' => 'users#edit', :as => 'user_root'
  resource :user do    
    resources :comments, :only => [:index], :controller => 'user/comments'
    resources :pictures, :only => [:index], :controller => 'user/pictures'
  end  

  match 'locations/:country' => 'locations#show', :as => 'locations_by_country'

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.  
  root :to => "static#index2"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
