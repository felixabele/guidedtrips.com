# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140331162356) do

  create_table "activities", :force => true do |t|
    t.integer "trip_id"
    t.integer "day"
    t.time    "time"
    t.text    "text"
    t.integer "location_id"
  end

  add_index "activities", ["day"], :name => "index_activities_on_day"
  add_index "activities", ["trip_id"], :name => "index_activities_on_trip_id"

  create_table "agencies", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "street"
    t.string   "city"
    t.string   "zip"
    t.string   "country"
    t.string   "phone"
    t.string   "email"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "authorizations", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.string   "token"
    t.string   "secret"
    t.string   "username"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comments", :force => true do |t|
    t.text     "body"
    t.integer  "vote"
    t.integer  "user_id"
    t.integer  "trip_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "countries", :force => true do |t|
    t.string  "code",           :limit => 2
    t.string  "name"
    t.string  "full_name"
    t.string  "iso3",           :limit => 3
    t.integer "number",         :limit => 3
    t.string  "continent_code", :limit => 2
  end

  add_index "countries", ["code"], :name => "index_countries_on_code"

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "locations", :force => true do |t|
    t.string  "country_code"
    t.string  "city"
    t.string  "accent_city"
    t.string  "region"
    t.integer "population"
    t.decimal "latitude",     :precision => 15, :scale => 10, :default => 0.0
    t.decimal "longitude",    :precision => 15, :scale => 10, :default => 0.0
    t.integer "country_id"
  end

  add_index "locations", ["accent_city"], :name => "index_locations_on_accent_city"
  add_index "locations", ["country_code"], :name => "index_locations_on_country"

  create_table "pictures", :force => true do |t|
    t.integer  "trip_id"
    t.integer  "user_id"
    t.boolean  "agency_image"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.boolean  "use_for_index"
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "trips", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "location_id"
    t.integer  "user_id"
    t.text     "included"
    t.text     "required"
    t.float    "price"
    t.integer  "min_participants"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.text     "excerpt"
    t.boolean  "is_published",     :default => false
    t.string   "slug"
  end

  add_index "trips", ["slug"], :name => "index_trips_on_slug", :unique => true

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "newsletter"
    t.string   "country"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "videos", :force => true do |t|
    t.string   "key"
    t.string   "source"
    t.integer  "user_id"
    t.integer  "trip_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "thumbnail"
    t.string   "title"
    t.text     "description"
    t.boolean  "is_active",         :default => true
    t.boolean  "belongs_to_agency"
  end

end
