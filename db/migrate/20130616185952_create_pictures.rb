class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.integer :trip_id
      t.integer :user_id
      t.boolean :agency_image
      t.attachment :file
      t.timestamps
    end
  end
end
