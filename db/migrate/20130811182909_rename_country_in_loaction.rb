class RenameCountryInLoaction < ActiveRecord::Migration
  def up
    rename_column :locations, :country, :country_code
  end

  def down
    rename_column :locations, :country_code, :country
  end
end
