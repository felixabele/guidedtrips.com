class AddUseForIndexAttributeToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :use_for_index, :boolean
  end
end
