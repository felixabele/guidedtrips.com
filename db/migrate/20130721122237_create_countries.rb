class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :code, :limit => 2
      t.string :name
      t.string :full_name
      t.string :iso3, :limit => 3
      t.integer :number, :limit => 3
      t.string :continent_code, :limit => 2
    end
    add_index :countries, :code
  end
end
