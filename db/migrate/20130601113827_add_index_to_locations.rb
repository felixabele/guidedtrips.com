class AddIndexToLocations < ActiveRecord::Migration
  def change
  	add_index :locations, :country 
  	add_index :locations, :accent_city
  end
end
