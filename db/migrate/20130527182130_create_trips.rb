class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :name
      t.text :description
      t.integer :location_id
      t.integer :user_id
      t.text :included
      t.text :required
      t.float :price
      t.integer :min_participants

      t.timestamps
    end
  end
end
