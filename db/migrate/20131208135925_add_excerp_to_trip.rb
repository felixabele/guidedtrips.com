class AddExcerpToTrip < ActiveRecord::Migration
  def change
    add_column :trips, :excerpt, :text
  end
end
