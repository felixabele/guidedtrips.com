class CreateAgencies < ActiveRecord::Migration
  def change
    create_table :agencies do |t|
      t.string :name
      t.string :description
      t.string :street
      t.string :city
      t.string :zip
      t.string :country
      t.string :phone
      t.string :email
      t.integer :user_id

      t.timestamps
    end
  end
end
