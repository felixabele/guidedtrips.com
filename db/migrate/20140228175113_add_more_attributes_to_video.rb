class AddMoreAttributesToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :thumbnail, :string
    add_column :videos, :title, :string
    add_column :videos, :description, :text
    add_column :videos, :is_active, :boolean, :default => true
  end
end
