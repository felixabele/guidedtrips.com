class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :key
      t.string :source
      t.integer :user_id
      t.integer :trip_id

      t.timestamps
    end
  end
end
