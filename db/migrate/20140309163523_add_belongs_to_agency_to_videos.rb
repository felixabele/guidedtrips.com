class AddBelongsToAgencyToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :belongs_to_agency, :boolean
  end
end
