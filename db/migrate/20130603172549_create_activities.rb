class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :trip_id
      t.integer :day
      t.time :time
      t.text :text
      t.integer :location_id

      #t.timestamps
    end
    add_index :activities, :trip_id
    add_index :activities, :day
  end
end
